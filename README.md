# Obtain Training Images (Python3)

The cone detection model in [QuickObjDetection](https://gitlab.com/KSU_EVT/autonomous-software/quickobjdetection), based on a re-trained version of yolov4-tiny, may struggle to detect cones that are not in a certain upright orientation.

To improve the quality and robustness of our image detection model, we must generate a few thousand images and label the traffic cones in each

There are 3 GoPro videos recorded from voltron practice run(s) named `GH010115.MP4` `GH010116.MP4` and `GH020115.MP4` on the team NextCloud

Using the following command with the [`ffmpeg`](https://ffmpeg.org/) tool, the [I-frames](https://en.wikipedia.org/wiki/Video_compression_picture_types) of each video were extracted and saved to `.jpg` files in the directories `GH010115` `GH010116` and `GH020115`:

```bash
ffmpeg -skip_frame nokey -i GH010115.MP4 -vsync 0 -frame_pts true %d.jpg
```

This resulted in about 7-8 thousand image files (too large for this repository)

Using the [`torchvision`](https://pytorch.org/vision/stable/index.html) python package, this script in this repository ([`obtain_training_images.py`](./obtain_training_images.py)) randomly shuffles and [transforms](https://cran.r-project.org/web/packages/torchvision/torchvision.pdf) these images, including cropping them, rotating (tilting) them slightly, and flipping them horizontally

We will then manually label these randomly-shuffled and transformed images using the [`LabelImg`](https://github.com/heartexlabs/labelImg) tool

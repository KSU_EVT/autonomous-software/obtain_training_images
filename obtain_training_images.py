#!/usr/bin/env python3
import sys
import os
import shutil
from pathlib import Path
import time
import datetime
import math
import random
import numpy as np

import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets

from PIL import *

def main():

    directory = os.getcwd()
    directory = os.path.expanduser(directory)
    directory = os.path.expandvars(directory)

    # homedir = Path.home()

    outpath = os.path.join(directory, "randomized")
    if not os.path.exists(outpath):
        os.mkdir(outpath)

    transform_inputs_img = transforms.Resize((512,512))
    transform_random_crop = transforms.RandomCrop(512)
    transform_randomhorizontalflip = transforms.RandomHorizontalFlip(p=0.5)
    transform_gaussian_blur = transforms.GaussianBlur(kernel_size=(3, 5), sigma=(0.6, 2.5))
    transform_color_jitter = transforms.ColorJitter(brightness=0.35, contrast=0.35)
    random_tilt = transforms.RandomRotation(20)


    out_files = []
    count = 0
    for root, dirs, files in os.walk(directory, onerror=None):
        for aFile in files:
            ext = aFile.split('.')[-1].lower()
            infile = os.path.join(root, aFile)
            if ext != 'jpg' or "randomized" in infile or aFile[0:1] == ".":
                continue
            outfile = os.path.join(outpath, str(count) + ".jpg")
            out_files.append(outfile)

            print(infile)
            print(outfile)

            inimg = Image.open(infile)

            outimg = random_tilt(inimg)
            outimg = transform_random_crop(outimg)
            outimg = transform_randomhorizontalflip(outimg)
            outimg = transform_color_jitter(outimg)
            outimg = transform_gaussian_blur(outimg)

            outimg.save(outfile)

            #clean up
            inimg.close()
            outimg.close()

            count += 1
    # end transform loop

    # shuffle the transformed images
    size = len(out_files)
    for i in range(0, size * 2):
        # pick two at random
        A = out_files[random.randint(0, size - 1)]
        B = out_files[random.randint(0, size - 1)]
        print(f'swap: {A} with {B}')
        if A == B:
            continue
        # swap the two images
        B_img = Image.open(B)
        temp_img = Image.open(A)
        B_img.save(A)
        temp_img.save(B)
        # clean up
        B_img.close()
        temp_img.close()



if __name__ == "__main__":
    main()
